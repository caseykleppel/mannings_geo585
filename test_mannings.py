import numpy as np
import nose
import mannings as mv

class TestManningsVel(object):
    
    @classmethod
    def setup_class(cls):
        '''this method is run once for each class before any tests are run'''
        cls.w = 2. 
        cls.S = 0.025 
        cls.n = 0.03
#       
        
    @classmethod
    def teardown_class(cls):
        """this method is run once for each class __after__ all tests are run"""
        pass
    
    def setUp(self):
        """this method is run once before __each__ tests is run"""
        pass
    
    def teardown(self):
        """this method is run once after __each__ tests is run"""
        pass   
    def test_init(self):
        
        Test = mv.Mannings(self)

        for i in range(4):
            Test.flow_depth.append(i+1)
            
        nose.tools.assert_equal(MBal.flow_depth, [1,2,3,4])
  
    def test_calculate_discharge(self):
        Test = mv.Mannings(self)
        S = 0.025
        n = 0.03 
        w = 2.0 
        y = 1.
        Qt= Test._calculate_discharge(S, n, w, y)
        expected_value =  6.64036698276408
        nose.tools.assert_almost_equal(expected_value, Qt, places=8)
        
    def test_dcalculate_discharge(self):
        Test = mv.Mannings(self)
        S = 0.025
        n = 0.03 
        w = 2.0 
        y = 1.
        dQ = Test._dcalculate_discharge(S, n, w, y)
        expected_value = 
        nose.tools.assert_almost_equal(expected_value, dQ, places=8)
        
    def test_solve_Mannings(self):
        S = 0.025
        n = 0.03 
        w = 2.0 
        Test = mv.Mannings(self)
      
        M_err =Test._solve_Mannings(S,n,w)
        nose.tools.assert_less_equal(abs(M_err), 1e-6)

    def test_run_Mannings(self):
        
        Test = mv.Mannings(self)
     
        S = 0.025
        n = 0.03 
        w = 2.0 
       
        depth, err = Test.run_Mannings(S,n,w)
        nose.tools.assert_true((abs(err) <=1e-6).all())
#        
