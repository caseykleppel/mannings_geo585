# -*- coding: utf-8 -*-
"""
Created on Mon Apr  6 15:45:34 2020

@author: Casey
"""

import numpy as np
class Mannings(object):
#*Static contstants!!*
    # These are defined outside the constructor __init__
    # and belong to the class. No copy of static variables is 
    # made when you create an object, all objects share the same copy of 
    # static variables
    def __init__(self,Q):
        '''Solves for flow depth for a rectangular stream channel 
        given discharge. 
        
        Parameters:
        Q : streamflow discharge (m**3/s)
        Returns None.'''
        
        self.Q = Q
        
        # Create lists to store generated values
        self.velocity = [] # (m/s) velocity of streamflow
        self.flow_depth = [] # Flow depth in rectangular, open flow streambed
        self.manningeq_errors = [] # solutions to solve function errors bounds?
        self.Qc = [] # time step discharge
        self.dQ = [] # dQdfd Discharge wrt flow depth: derivative values per time step
        self.disch = [] #disch values 
        

         
                
        
        
    def _calculate_discharge(self, S, n, w, y):
            '''Returns discharge (m**3/s) for a rectangular channel
            :param n: Mannings roughness coefficient (s/m**1/3)
            :param S: Slope of rectangular channel (m/m)
            :param w: width of channgel (m)
            :param Q: discharge (m**3/s)
            :state variable y: rectangular channel depth (m)
            :returns: discharge Qc"
            '''
            
            R = (y * w)/(2. * y + w) # hydraulic radius 
            Qc = ((R**(2/3) * np.sqrt(S))/n) * y * w # (m**3/s)
            self.Qc.append(Qc)
            return Qc
        
        
    def _dcalculate_discharge(self, S, n, w, y):
            '''Returns derivative of discharge for a rectangular channel
            :param n: Mannings roughness coefficient (s/m**1/3)
            :param S: Slope of rectangular channel (m/m)
            :param w: width of channgel (m)
            :param Q: discharge (m**3/s)
            :state variable y: rectangular channel depth (m)
            :returns: dQ_dy"
            '''
            
            calc_disch = self.Qc[-1]
            dQ_dy = calc_disch * ((6*y + 5*w) / ( 3*y * (2*y + w)))
            self.dQ.append(dQ_dy) 
            return dQ_dy
        
        
    def _solve_Mannings(self, S, n, w):
            y_old = 1
            y = y_old
            Mannings_err = np.array([1])
            i = 0
            # Start Newton-Raphson loop. Stop when Mannings error is smaller than 1e-6
            # or number of iterations exceeds 500
            while (abs(Mannings_err) >= 1e-6).any() and ( i < 500):
                Mannings_err = self._calculate_discharge(S, n, w, y) - self.Q
                dMannings_err = self._dcalculate_discharge(S, n, w, y)
                # Newton Raphson method to update depth
                y = y - (Mannings_err/dMannings_err) 
                i += 1
                d = (((((y * w)/(2. * y + w))**(2/3)) * np.sqrt(S))/n) * y * w
                v = self.Q/(w*y)
                self.disch.append(d)
                self.velocity.append(v)
                self.flow_depth.append(y)
                return Mannings_err
            
            
    def run_Mannings(self, S, n, w):
                """Solution to Mannings eq using streambed characteristics
                :param n: Mannings roughness coefficient (s/m**1/3)
                :param S: Slope of rectangular channel (m/m)
                :param w: width of channgel (m)
                """
                M_err = self._solve_Mannings(S, n, w)
                self.manningeq_errors.append(M_err)
                return self.flow_depth, self.velocity, self.disch